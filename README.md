# castle-koa
a nodejs mvc framework base on koa2
>一个基于Koa2的Nodejs的MVC框架
## auther castle/490523604@qq.com

## use 使用方法
### use castle_koa_cli 使用castle_koa_cli工具
```bash
# 你需要先安装该工具 you should install it 
npm i -g castle_koa_cli
# 然后使用命令行工具完成项目初始化工作 then use cli tools finish your project init
castle-cli init server your_directory
# 进入到项目目录中使用npm i
cd your_directory 
npm i
# 现在你可以npm start来启动项目了，
```

[Controller 控制器使用说明](./doc/Controller.md)


## 生成器使用说明

### 生成器规则调整，生成的controller方法和Relation放在独立的目录中，检测实际执行目录是否存在执行逻辑文件，若存在则不替换，否则替换掉。

## 数据库事务
```typescript
async test() {
        //获取任意一个模型或使用 let trans = await this._ctx.config.db.transaction()
        let Model = this.M('Test')
        try {
            let trans = Model.startTrans()
            this.M('AB').setTrans(trans)
            await Model.add({ S: 2 })
            if (Math.floor(Math.random() * 10) % 2 == 1) {

                await trans.commit()
                console.log('commit')
                return await Model.select()
            } else {

                await trans.rollback()
                console.log('rollback')
                return await Model.select()
            }
        } catch (error) {
            //使用trans或者Model的commit或rollback都行
            Model.rollback()
        }
    }
```