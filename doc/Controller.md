# 控制器部分
所有控制器必须继承自BaseController，如果该控制器需要提供基础的CURD操作，则继承自Controller
## BaseController基础方法
### 鉴权 _checkPermission(ctx)
该方法会在类初始化的时候被调用，传入参数为ctx，请针对ctx进行判断，返回true表示通过，若不通过这可以返回false或直接throw new Error来抛出异常，该异常会直接返回到前端
### 通过文件路径发送下载文件 _send(path: string, filename: string)
### 通过传入内容来发送下载文件 _sendContent(content: any, filename: string): void
### 操作cookie _cookie(name: string, value?: string, options?: CookieOptions)
```typescript
interface CookieOptions {
    domain?: string,
    path?: string,
    maxAge?: number,
    expires?: Date,
    httpOnly?: boolean,
    overwrite?: boolean
}
```
```typescript
//设置cookie
this._cookie('a',1)
// 读取cookie
this._cookie('a')
//销毁cookie
this._cookie('a',null)
```
### 操作session _session(name: string, value?: string)
```typescript
//设置
this._session('a',1)
// 读取
this._session('a')
//销毁
this._session('a',null)
```
### 获得Model数据库操作对象 M(表名):Model
该方法需要传入一个表名，默认为当前对象的_ModelName名称，可以自由设定
```typescript
this.M()
```
### 获得Relation关系对象操作对象 R(关系名称):Relation
该方法需要传入一个关系名，默认为当前对象的_ModelName名称，可以自由设定
```typescript
this.R()
```
### 从POST中获取参数 I('参数表达式')
```typescript
// $_POST['A']
this.I('A')
```

## Controller基础方法

### 添加 add
### 修改 save
### 删除 del
### 查询 search
### 批量修改 saveW
### 批量添加 adds
### 批量删除 delW
该方法提供
### 批量添加或修改 replaceW
该方法提供了可以不需要主动判断该数据记录是否存在的操作方法，允许用户直接一次性添加或修改多条数据，判断重复的规则为数据库的唯一键设定规则。使用了MySQL的replace语句。
#### 请求参数
> W 批量替换或添加的限制性条件，需要符合W参数规范
> Data 批量添加或替换的数据部分，需要符合该类的结构规范