import { uniq, intersection, forOwn } from 'lodash'
import { Op } from 'sequelize'
import { M, R } from '../utils/db';
import basecontroller from './basecontroller';
export default class Controller extends basecontroller {
    async search(post: any) {
        let W = this.I('W', { d: {}, type: 'object' }),
            Keyword = this.I('Keyword', { d: '', type: 'string' }),
            KeywordFields = this.I('KF', { d: [], type: 'array' }),
            P = this.I('P', { d: 1, type: 'number' }),
            N = this.I('N', { d: 10, type: 'number' }),
            Sort = this.I('Sort', { d: '', type: 'string' }),
            WPKIDs: any[] = [],
            PKIDs: any[] = [],
            KeywordIDs: any[] = []
        if (Keyword.length > 0) {
            let Where = {};
            let Fields: string[] = uniq([...KeywordFields, ...this._KeywordFields])
            if (Fields) {
                Fields.forEach((v: string) => {
                    Where[v] = { like: `%${Keyword.replace(/[ ;%\r\n]/g, '')}%` }
                })
                if (this._KeywordTable) {
                    KeywordIDs = await M(this._ctx, this._KeywordTable).where({ or: Where }).getFields(this._ctx.config.getDbTablePK(this._ModelName), true)
                }
            }
        }
        if (Object.keys(W).length > 0) {

        }
        let ModelName = this._WTable ? this._WTable : this._ModelName;
        let CurrentModel = M(this._ctx, ModelName)
        WPKIDs = await CurrentModel.where(W).order(Sort).getFields(this._ctx.config.getDbTablePK(ModelName), true)
        if (Keyword) {
            //当且仅当Keyword不为空的时候才做查询结果合并
            PKIDs = intersection(WPKIDs, KeywordIDs)
        } else {
            PKIDs = WPKIDs;
        }
        let T = PKIDs.length;
        if (PKIDs.length > (P - 1) * N) {
            PKIDs = PKIDs.slice((P - 1) * N, P * N)
        } else {
            PKIDs = [];
        }
        return {
            L: PKIDs.length > 0 ? await R(this._ctx, ModelName).order(Sort).fields(Object.keys(this._searchFields)).objects(PKIDs) : [],
            T,
            P, N, R: {}
        }
    }
    async add(post, ctx) {
        return await this._model.add(post)
    }
    async del(post, ctx) {
        return await this._model.where({ [this._ctx.config.getDbTablePK(this._ModelName)]: this.I(this._ctx.config.getDbTablePK(this._ModelName), { type: 'number' }) }).del();
    }
    async save(post, ctx) {
        return await this._model
            .where({ [this._ctx.config.getDbTablePK(this._ModelName)]: this.I(this._ctx.config.getDbTablePK(this._ModelName), { type: 'number' }) })
            .save(this.I('Params', {
                type: (data: any) => {
                    //TODO 循环处理判定数据是否合法及函数过滤
                    forOwn(this._saveFields, (v: any, k: string) => {
                        if (v instanceof Function) {
                            v(k, data)
                        } else {

                        }
                    })
                    return true;
                }
            }))
    }
    async saveW(post, ctx) {
        let W = this.I('W', { type: 'object' });
        if (W)
            return await this._model.where(this.I('W', { type: 'object' })).save(this.I('Params', { type: 'object' }))
        return false;
    }
    async delW(post, ctx) {
        let W = this.I('W', { type: 'object' });
        if (W)
            return await this._model.where(W).del();
        else
            return false;
    }
    async adds(post, ctx) {
        return await this._model.addAll(Object.values(post))
    }
    async replaceW(post, ctx) {
        await this._model.where(this.I('W', { type: 'object', d: { [this._pk]: 0 } })).del()
        return await this._model.addAll(this.I('Data'))
    }
}