import Model from './model'
import * as _ from 'lodash'
import { M, R } from '../utils/db'
import { array_columns } from '../utils/array'
import ErrorType from '../utils/errors';
export class RelationConfiger {
    name: string = ''
    table: string = ''
    fields: string[] | Function | string = []
    pk: string = ''
    fk?: string = ''
    relation?: boolean | string | Relation = false
    where?: Object | Function = undefined
    order?: string | Function = ''
    constructor(config?: RelationConfiger) {
        if (config) {
            if (config.name) this.name = config.name
            if (config.table) this.table = config.table
            if (config.fields) {
                if ('string' == typeof config.fields) {
                    this.fields = config.fields.split(',')
                } else {
                    this.fields = config.fields
                }
            }
            if (config.pk) this.pk = config.pk
            this.fk = config.fk ? config.fk : config.pk
            this.relation = config.relation ? config.relation : false
            this.where = config.where ? config.where : undefined
        }
    }
}
export default class Relation {
    protected _one: { [index: string]: RelationConfiger } = {};
    protected _many: { [index: string]: RelationConfiger } = {};
    protected _extend: { [index: string]: RelationConfiger } = {};
    protected _table = "";
    protected _pk = "";
    protected _fields: Array<string> = [];
    protected _model: Model;
    protected _controller;
    protected _foreach: any = [];
    protected _ctx: any = {};
    /**
     *
     * @param Table 表名
     * @param Fields 字段列表
     * @param PK 主键
     */
    public constructor(ctx, Table, Fields: string | Array<string> = "", PK = "") {
        this._table = Table;
        this._ctx = ctx;
        let _fields = [];
        if (Fields instanceof String) {
            _fields = Fields.split(',')
        }
        if (Fields.length == 0) {
            _fields = this._ctx.config.getDbTableFields(Table)
        }
        if (PK.length === 0) {
            PK = this._ctx.config.getDbTablePK(Table)
        }
        this._fields = _fields;
        this._pk = PK;
        this._model = M(ctx, Table);
    }

    /**
     * 可以不通过构造函数来添加属性信息
     * @param Table
     * @param Fields
     * @param PK
     */
    public config(Table, Fields, PK) {
        this._table = Table;
        this._fields = Fields;
        this._pk = PK;
        if (Table)
            this._model = M(this._ctx, Table);
    }
    /**
     * 拥有一个
     * @param name 关系名称
     * @param Table 表名称
     * @param Fields 要取的字段范围
     * @param TableField 子表字段
     * @param MainField 主表字段
     */
    public hasOne(config: RelationConfiger) {
        if (_.isString(config.name) && config.name.length > 0) {
            this.One = config;
        }
        return this;
    }
    get One() {
        return this._one;
    }
    set One(config: RelationConfiger | any) {
        if (_.isString(config.name) && config.name.length > 0) {
            this._one[config.name] = new RelationConfiger(config)
        }
    }
    /**
     * 有多个配置
     * @param config
     * @returns {boolean}
     */
    public hasMany(config: RelationConfiger) {
        this.Many = config;
        return this;
    }
    get Many() {
        return this._many;
    }
    set Many(config: RelationConfiger | any) {
        if (_.isString(config.name) && config.name.length > 0) {
            this._many[config.name] = new RelationConfiger(config)
        }
    }

    /**
     * 扩展字段配置
     * @param config
     * @returns {boolean}
     */
    public extend(config: RelationConfiger) {
        if (_.isString(config.name) && config.name.length > 0) {
            this.Extend = config;
        }
        return this;
    }

    /**
     *
     * @returns {any}
     * @constructor
     */
    get Extend() {
        return this._extend;
    }

    /**
     *
     * @param config
     * @constructor
     */
    set Extend(config: RelationConfiger | any) {
        if (_.isString(config.name) && config.name.length > 0) {
            this._extend[config.name] = new RelationConfiger(config)
        }
    }
    public eval(w: any) {
        if (w instanceof Function) {
            return w(this._ctx);
        } else {
            return w;
        }
    }
    // set Fields(fields){
    //     this._fields=fields;
    // }
    // get Fields(){
    //     return this._fields;
    // }
    /**
     * 获取对象
     * @param {Array<Number>} PKValues
     * @returns {any}
     */
    public async objects(PKValues: Array<Number>) {
        if (PKValues instanceof Array) {
            let data = await this._model.fields(this._fields).where({
                [this._pk]: { 'in': PKValues }
            }).select()
            //开始循环属性配置并生成相关。。
            let Qs: any = [data];
            if (data instanceof Array) {
                // data = JSON.parse(JSON.stringify(data))
                _.forOwn(this.One, (v, k) => {
                    if (v instanceof Function) {
                        try {
                            v = v(data)
                        } catch (error) {

                        }
                    }
                    if (v.relation instanceof Relation) {
                        Qs.push(v.relation.fields(this.eval(v.fields)).order(this.eval(v.order)).where(this.eval(v.where)).where({ [v.fk ? v.fk : v.pk]: { 'in': array_columns(data, v.pk, true) } }).select())
                    } else if (!_.isString(v.relation)) {
                        Qs.push(M(this._ctx, v.table).order(this.eval(v.order)).fields(this.eval(v.fields)).where(this.eval(v.where)).where({ [v.fk ? v.fk : v.pk]: { 'in': array_columns(data, v.pk, true) } }).select())
                    } else {
                        let r = R(this._ctx, v.relation);
                        if (r instanceof Relation)
                            Qs.push(r.fields(this.eval(v.fields)).order(this.eval(v.order)).where(this.eval(v.where)).where({ [v.fk ? v.fk : v.pk]: { 'in': array_columns(data, v.pk, true) } }).select())
                    }
                })
                _.forOwn(this.Many, (v, k) => {
                    if (v instanceof Function) {
                        try {
                            v = v(data)
                        } catch (error) {

                        }
                    }
                    if (v.relation instanceof Relation) {
                        Qs.push(v.relation.fields(this.eval(v.fields)).order(this.eval(v.order)).where(this.eval(v.where)).where({ [v.fk ? v.fk : v.pk]: { 'in': array_columns(data, v.pk, true) } }).select())
                    } else if (!_.isString(v.relation)) {
                        Qs.push(M(this._ctx, v.table).order(this.eval(v.order)).fields(this.eval(v.fields)).where(this.eval(v.where)).where({ [v.fk ? v.fk : v.pk]: { 'in': array_columns(data, v.pk, true) } }).select())
                    } else {
                        let r = R(this._ctx, v.relation);
                        if (r instanceof Relation)
                            Qs.push(r.fields(this.eval(v.fields)).order(this.eval(v.order)).where(this.eval(v.where)).where({ [v.fk ? v.fk : v.pk]: { 'in': array_columns(data, v.pk, true) } }).select())
                    }
                })
                _.forOwn(this.Extend, (v, k) => {
                    if (v instanceof Function) {
                        try {
                            v = v(data)
                        } catch (error) {

                        }
                    }
                    if (v.relation instanceof Relation) {
                        Qs.push(v.relation.fields(this.eval(v.fields)).order(this.eval(v.order)).where(this.eval(v.where)).where({ [v.fk ? v.fk : v.pk]: { 'in': array_columns(data, v.pk, true) } }).select())
                    } else if (!_.isString(v.relation)) {
                        Qs.push(M(this._ctx, v.table).order(this.eval(v.order)).fields(this.eval(v.fields)).where(this.eval(v.where)).where({ [v.fk ? v.fk : v.pk]: { 'in': array_columns(data, v.pk, true) } }).select())
                    } else {
                        let r = R(this._ctx, v.relation);
                        if (r instanceof Relation)
                            Qs.push(r.fields(this.eval(v.fields)).order(this.eval(v.order)).where(this.eval(v.where)).where({ [v.fk ? v.fk : v.pk]: { 'in': array_columns(data, v.pk, true) } }).select())
                    }
                })
            }
            let result = await Promise.all(Qs)
            let i = 1, datae = result[0], one = {}, many = {}, extend = {}, config = {};
            _.forOwn(this._one, (v, k) => {
                one[v.name] = { values: result[i], config: v };
                i++;
            })
            _.forOwn(this._many, (v, k) => {
                many[v.name] = { values: result[i], config: v };
                i++;
            })
            _.forOwn(this._extend, (v, k) => {
                extend[v.name] = { values: result[i], config: v };
                i++;
            })
            _.forOwn(datae, (v, k) => {
                _.forOwn(one, (d: any, f) => {
                    let s = _.filter(d.values, { [d.config.fk]: datae[k][d.config.pk] });
                    datae[k][f] = s.length > 0 ? s[0] : {}
                })
                _.forOwn(many, (d: any, f) => {
                    let s = _.filter(d.values, { [d.config.fk]: datae[k][d.config.pk] });
                    datae[k][f] = s.length > 0 ? s : []
                })
                _.forOwn(extend, (d: any, f) => {
                    //TODO 检查所查询数据不存在的时候追加空数据
                    let s = _.filter(d.values, { [d.config.fk]: datae[k][d.config.pk] });
                    datae[k] = _.assign(v, s.length > 0 ? s[0] : {});
                })
                _.forOwn(this._foreach, (f: any, k: any) => {
                    if (_.isFunction(f)) {
                        f.apply(this, [datae[k]])
                    }
                })
            })
            return datae;
        } else {
            throw new Error(ErrorType.RELATION_OBJECTS_VALUE_NOT_ARRAY)
        }
    }
    protected data_columns(arr: any, column: any) {
        let a = [];
        _.forOwn(arr, (v, k) => {
            a.push(v.dataValues[column])
        })
        return a
    }

    /**
     * 取出
     * @param arr
     * @returns {Array}
     */
    protected dataValues(arr, column: any = false) {
        let a = [];
        _.forOwn(arr, (v, k) => {
            if (false == column)
                a.push(v.dataValues)
            else
                a.push(v.dataValues[column])
        })
        return a
    }
    public where(where) {
        this._model.where(where)
        return this;
    }

    public load(DbConfig: {} | string = null, ) {
        var conf = {};
        if (DbConfig instanceof String) {
            //从定义目录加载
            conf = require(`${this._ctx.config.MODEL_PATH}/${DbConfig}`);
        } else if (_.isObject(DbConfig)) {

        }

    }
    public fields(fields) {
        this._fields = fields;
        return this;
    }
    public async selectAndCount() {
        let d = await this._model.fields([this._pk]).selectAndCount()
        if (d.rows.length == 0) {
            return [[], 0];
        }
        var PKs = array_columns(d.rows, this._pk);
        return Promise.all([
            this.objects(PKs),
            d.count
        ]);
    }
    public order(order) {
        this._model.order(order)
        return this;
    }
    public async save(data) {
        return await this._model.save(data).then(c => {
            return c > -1;
        })
    }
    public page(page, number) {
        this._model.page(page, number)
        return this;
    }
    public async add(data) {
        return await this._model.add(data).then(d => {
            if (_.isObject(d) && d[this._pk] > 0) {
                return this.objects([d[this._pk]]).then(p => {
                    return p[0];
                })
            } else {
                return d;
            }
        })
    }
    // public find(){
    //     return this._model.find()
    // }
    public async del() {
        return await this._model.del();
    }
    public async select() {
        return await this._model.fields([this._pk]).select().then(d => {
            if (d instanceof Array && d.length > 0) {
                var PKs = array_columns(d, this._pk);
                return this.objects(PKs);
            } else {
                return [];
            }
        })
    }

    /**
     * 查询一个
     */
    public async find() {
        return await this._model.getFields(this._pk).then(d => {
            if (_.isNumber(d) && d > 0) {
                return this.objects([d]).then(data => {
                    return data instanceof Array && data.length > 0 ? data[0] : {}
                });
            } else {
                return {};
            }
        })
    }
}
