import { ctx } from "./iface";
import { WriteStream, createWriteStream } from "fs";
import * as moment from 'moment'
const logconfig: {
    writeStream?: WriteStream,
    fspath: string
} = {
    fspath: ''
}
export class Log {
    static write(content: string) {
        try {
            let fspath = moment().format('YYYY-MM-DD.log')
            if (fspath != logconfig.fspath) {
                if (logconfig.writeStream) { logconfig.writeStream.close() }
                logconfig.writeStream = undefined
            }
            if (!logconfig.writeStream) {
                logconfig.writeStream = createWriteStream(fspath)
            }
        } catch (error) {

        } finally {
            try {
                if (logconfig.writeStream.writable) {
                    logconfig.writeStream.write(content)
                }
            } catch (error) {

            }
        }
    }
    static log(path: string, start: Date, end: Date, ) { }
    static logRaw(content: string) {
        this.write(content)
    }
}
export class WSLog extends Log {
    static write() { }
}
export class HTTPLog extends Log {

}