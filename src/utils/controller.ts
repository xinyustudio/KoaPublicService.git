import ErrorType from "../utils/errors";

/**
 *  取参数,支持传入typeof值或者是函数类型来完成安全性校验
 * @param ctx 
 * @param name 
 * @param options 
 */
export function I(ctx: any, name: string, options?: { type?: String | Function | Symbol, d?: any }) {
    let data = 'undefined' !== typeof ctx.req.body[name] ? ctx.req.body[name] : (options ? options.d : undefined);
    if (!options) { return data }
    if (options.type instanceof String) {
        if (options.type == typeof data) {
            return data;
        } else {
            throw new Error(ErrorType.POST_TYPE_ERROR + '_' + name)
        }
    } else if (options.type instanceof Function) {
        if (options.type(data)) { return data } else { throw new Error(`PARAM_${name}_TYPE_ERROR`) }
    } else if (options.type instanceof Symbol) {
        return data;
    }
    else {
        return data;
    }
}