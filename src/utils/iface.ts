import Config from "../config";
import * as Sequelize from 'sequelize';
export interface UploadFiles {
    fieldName: string,
    name: string,
    originalFilename: string,
    path: string,
    size: number,
    type: string
}
export enum HttpCode {
    NOT_FOUND = 404,
    OK = 200,
    PERMISSION_DENIED = 403,
    SERVER_ERROR = 500
}
export interface ctx {
    error: any,
    config: Config,
    path: string,
    websocket?: any,
    set(key: string, value: string | number)
    get(key: string)
    req: {
        body: { [index: string]: any },
        files: { [index: string]: UploadFiles }
    },
    method: string,
    request: { body: any, query: { [index: string]: string } },
    control: { m: string, c: string }
    attachment(filename: string)
    redirect(w: string, h?: string)
    header: {
        [index: string]: string,
    }
    headers: {
        [index: string]: string,
    }
    hostname: string
    body: any
    cookies: { set: (name: string, value: any, options?: any) => {}, get: (name: string) => string }
    session: any,
    status: number
    query: { [index: string]: string }
}
export const DbDataType = {
    char: Sequelize.CHAR,
    varchar: Sequelize.STRING,
    double: Sequelize["DOUBLE PRECISION"],
    float: Sequelize.FLOAT,
    text: Sequelize.TEXT,
    smallint: Sequelize.SMALLINT,
    tinyint: Sequelize.TINYINT,
    mediumint: Sequelize.MEDIUMINT,
    int: Sequelize.INTEGER,
    bigint: Sequelize.BIGINT,
    decimal: Sequelize.DECIMAL,
    boolean: Sequelize.BOOLEAN,
    enum: Sequelize.ENUM,
    datetime: Sequelize.DATE,
    timestamp: Sequelize.TIME,

}
export const DbOp = {
    eq: Sequelize.Op.eq,
    ne: Sequelize.Op.ne,
    gte: Sequelize.Op.gte,
    gt: Sequelize.Op.gt,
    lte: Sequelize.Op.lte,
    lt: Sequelize.Op.lt,
    not: Sequelize.Op.not,
    is: Sequelize.Op.is,
    in: Sequelize.Op.in,
    notIn: Sequelize.Op.notIn,
    like: Sequelize.Op.like,
    notLike: Sequelize.Op.notLike,
    iLike: Sequelize.Op.iLike,
    notILike: Sequelize.Op.notILike,
    regexp: Sequelize.Op.regexp,
    notRegexp: Sequelize.Op.notRegexp,
    iRegexp: Sequelize.Op.iRegexp,
    notIRegexp: Sequelize.Op.notIRegexp,
    between: Sequelize.Op.between,
    notBetween: Sequelize.Op.notBetween,
    overlap: Sequelize.Op.overlap,
    contains: Sequelize.Op.contains,
    contained: Sequelize.Op.contained,
    adjacent: Sequelize.Op.adjacent,
    strictLeft: Sequelize.Op.strictLeft,
    strictRight: Sequelize.Op.strictRight,
    noExtendRight: Sequelize.Op.noExtendRight,
    noExtendLeft: Sequelize.Op.noExtendLeft,
    and: Sequelize.Op.and,
    or: Sequelize.Op.or,
    any: Sequelize.Op.any,
    all: Sequelize.Op.all,
    values: Sequelize.Op.values,
    col: Sequelize.Op.col,
    placeholder: Sequelize.Op.placeholder,
    join: Sequelize.Op.join
}