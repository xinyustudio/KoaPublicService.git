import * as path from 'path'
import * as fs from 'fs'
import Config from '../config';
/**
 * 获取配置类
 */
export function get_configer(): Config {
    let configPath = path.resolve('dist/config/index.js')
    if (fs.existsSync(configPath)) {
        return require(configPath).default;
    }
    return require('../config').default
}