
import * as crypto from 'crypto'
/**
 * 密码加密
 * @param pwd
 * @returns {string}
 */
export function password_hash (pwd):string {
    const md5 = crypto.createHash('md5');
    return md5.update(pwd).digest('hex').toLowerCase();
}
/**
 * 密码验证
 * @param pwd
 * @param sign
 * @returns {boolean}
 */
export function password_verify (pwd,sign):boolean {
    const md5 = crypto.createHash('md5');
    return md5.update(pwd).digest('hex').toLowerCase()==sign;
}
