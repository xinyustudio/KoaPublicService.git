import Model from '../lib/model'
import * as _ from 'lodash'
import Relation from '../lib/relation'
import { ctx } from './iface';
import * as path from 'path'
export function M(ctx: ctx | any, Table): Model {
    if (ctx.config.getDbDefine(Table))
        return new Model(ctx, Table);
}
/**
*
* @param name
* @param {boolean} force
* @returns Relation
* @constructor
*/
export function R(ctx: ctx, name: string, force: boolean = false): Relation {
    const Relations = {};
    if (!_.isObject(Relations[name])) {
        try {
            let rp = path.resolve(path.join(ctx.config.AppPath, 'relation', name))
            var r = require(rp).default;
            if (r) {
                Relations[name] = new r(ctx, name, ctx.config.getDbTableFields(name), ctx.config.getDbTablePK(name));
            } else {
                Relations[name] = new Relation(ctx, name, ctx.config.getDbTableFields(name), ctx.config.getDbTablePK(name));
            }
        } catch (e) {
            Relations[name] = new Relation(ctx, name, ctx.config.getDbTableFields(name), ctx.config.getDbTablePK(name));
        }
    }
    return Relations[name];
}


