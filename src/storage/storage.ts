export default interface storage {
    write(path: string, data: string | Buffer | number): Promise<number | boolean>;
    read(p: string): Promise<string>
    readAsBuffer(path: string): Promise<Buffer>
    mkdir(path: string, mode: number): Promise<boolean>
    unlink(path: string): Promise<boolean>
    ls(path: string): Promise<string[]>
    isDirectory(path: string): Promise<boolean>
}