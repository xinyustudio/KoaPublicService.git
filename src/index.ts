import * as koa from 'koa'
import middlewares from './middlewares'
import controller from './lib/controller';
import relation from './lib/relation';
import model from './lib/model';
import config from './config/index'
import basecontroller from './lib/basecontroller'
import { parse } from 'url';
import { request } from 'https';
// import * as WebSocket from 'ws'
import * as WebSocket from 'koa-websocket'
export const ServerConfig = {
    WebSockets: [],
    //WebSocket 处理服务
    WSHanders: {}
}
export const app = new WebSocket(new koa());
Date.prototype.toJSON = function () { return this.toLocaleString(); }
middlewares(app)
// middlewares(app.ws)
export default app;
export const Controller = controller
export const Relation = relation
export const Model = model
export const Config = config
export const BaseController = basecontroller